# Node Mongo Starter

NodeJS, ExpressJS, MongoDB - Boilerplate

## Installation

1. Clone the repo
	
	```
	$ git clone https://bitbucket.org/ketancrest/node-mongo-starter.git
	```

2. Install NPM packages
	
	```
	$ cd node-mongo-starter && npm install
	```

3. Copy `.env.example` to `.env`
	
	```
	$ cp .env.example .env
	```


4. Run Server
	
	```
	$ npm start
	```
